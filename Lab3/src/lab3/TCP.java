/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;


/**
 *
 * @author marcin
 */
public class TCP {
    
    Socket requsetSocket;
    BufferedReader reader;
    BufferedReader userInput;
    PrintWriter printWriter;
    
    /*
    Metoda odpowiadająca za podłączenie się do portu 13 za pomocą pakietów TCP
    obsługującego domyślnie zwrot daty.
    */
    
    public void connectPort13(String host)
    {
        try {
            requsetSocket = new Socket(host, 13); // Ustawianie soketu do odbierania danych
            reader = new BufferedReader(new InputStreamReader(requsetSocket.getInputStream())); // Utowrzenie obiektu do odczytywania danych z przychodzących do soketa
            
            System.out.println("Data: " + reader.readLine()); // Wypisywanie danych
            
            reader.close(); //Zamykanie połączenia
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }   
    }
    /*
    Metoda odpowiadająca za podłączenie się do portu 13 za pomocą pakietów UDP
    obsługującego domyślenie funkcję echo
    */
    public void connectPort7(String host)
    {
        try {
            requsetSocket = new Socket(host, 7); // Ustawianie soketu do odbierania/wysyłania danych
            
            reader = new BufferedReader(new InputStreamReader(requsetSocket.getInputStream())); // Utowrzenie obiektu do odczytywania danych przychodzących do soketa
            userInput = new BufferedReader(new InputStreamReader(System.in)); //Obiekt do oczytywania znaków wprowadzonych przez użytkownika
            printWriter = new PrintWriter(requsetSocket.getOutputStream()); // Obiekt do wysyłania danych
            
            System.out.println("To exit press '.'");
            
            while (true) {
                System.out.println("Client: ");
                String theLine = userInput.readLine(); // odczytanie znaków wprowadzonych przez użytkownika
                if (theLine.equals(".")) // zatrzymanie programu jeżeli naciśnie '.'
                    break;
                printWriter.println(theLine); // Wysłanie znaków
                printWriter.flush(); // Wyczyszczenie obiektu
                System.out.println("Serwer: " + reader.readLine()); // Wypisanie odpowiedzi serwera
            }
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
    }
}
