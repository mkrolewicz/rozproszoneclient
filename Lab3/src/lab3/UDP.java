/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 *
 * @author marcin
 */
public class UDP {
    
    DatagramSocket socket;
    DatagramPacket packet;
    DatagramPacket reply;
    InetSocketAddress socketAddress;
    
    String response;
    BufferedReader userInput;
    /*
    Metoda odpowiadająca za podłączenie się do portu 13 za pomocą pakietów UDP
    obsługującego domyślnie zwrot daty.
    */
    public void ConnectPort13(String host){
        try {
            socket = new DatagramSocket(); // Tworzenie soketu do wysyłania danych
            socketAddress = new InetSocketAddress(host,13); // Tworzenie adresu dla soketa
            socket.setSoTimeout(2000); // 
            
            byte[] buffer = new byte[1024]; // bufor do przetrzymywania odpowiedzi serwera 
            packet = new DatagramPacket(buffer, buffer.length, socketAddress); // Tworzenie pakietu
            
            packet.setLength(1); // Ustawianie długości pakietu
            socket.send(packet); // Wysyłanie krótkiego pakietu
            
            packet.setLength(buffer.length); // Ustawianie długości pakietu na długość bufora; przygotowanie do odebrania daty
            socket.receive(packet); // Odebranie pakietu
            
            response = new String(buffer,0,packet.getLength(),"US-ASCII"); // Konwersja bufora na stringa
            
            System.out.println("Data: " + response); // Wypisanie odpowiedzi
            
            socket.close(); //Zamknięcie połączenia
            
        } catch (IOException ioexp) {
            ioexp.printStackTrace();
        }      
    }
    /*
    Metoda odpowiadająca za podłączenie się do portu 13 za pomocą pakietów UDP
    obsługującego domyślenie funkcję echo
    */
    public void ConnectPort7(String host){
        
        userInput = new BufferedReader(new InputStreamReader(System.in)); // obiekt do oczytywania co wpisuje użytkownik
        
        try {
            socket = new DatagramSocket(); // Tworzenie soketu do wysyłania danych
            socketAddress = new InetSocketAddress(host,7); //Tworzenie adresu dla soketa
            System.out.println("To exit press '.' "); //Gdy użytkownika naciśnie '.' program się zatrzymuje.
            while (true) {
                
                System.out.println("Client: ");
                response = userInput.readLine(); // Pobieranie wprowadzonych znakow
                if (response.equals(".")) // Sprawdzanie czy użytkownik wcisnął '.'
                    break;
                
                byte[] buffer = response.getBytes(); // konwersja ze stringa na bajty
                
                packet = new DatagramPacket(buffer, buffer.length,socketAddress); // Tworzenie pakietu do wysłania
                socket.send(packet); // Wysyłanie pakietu
                
                buffer = new byte[65536]; //Tworzenie bufora dla przychodządzego pakietu
                reply = new DatagramPacket(buffer, buffer.length); // tworzenie pakietu zwrotnego
                socket.receive(reply); // odbieranie pakietu
                
                byte[] data = reply.getData(); //przerabianie pakietu na bajty
                response = new String(data,0,reply.getLength()); // przerabianie bajtow na string
                System.out.println("Serwer: " + response); // wypisywanie obpowiedzi serwera
                
            }
            
            socket.close(); // Zamknięcie połączenia
            
        } catch (IOException ioexp) {
            ioexp.printStackTrace();
        }
    }
}
